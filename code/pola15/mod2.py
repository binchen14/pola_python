def rk45(xi0,Y0,h0,hmax,M,a,epsilon=1e-10):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        and adaptive stepsize control.
        h0 input step size, might be < 0, need be very careful,
        return [xi1, Y1, h1] (new stepsize) """

    # Cash-Karp parameters
    # a = [0.0, 0.2, 0.3, 0.6, 1.0, 0.875] not used, Y_prime(xi,Y) not depending on xi
    b = [[],
         [0.2],
         [0.075,        0.225 ],
         [0.3,            -0.9,        1.2      ],
         [-11.0/54.0,     2.5,         -70.0/27.0,     35.0/27.0],
         [1631.0/55296.0, 175.0/512.0, 575.0/13824.0,  44275.0/110592.0, 253.0/4096.0]]
    c  = [37.0/378.0,     0.0,         250.0/621.0,     125.0/594.0,      0.0,        512.0/1771.0]
    #d = [2825.0/27648.0  0.0	       18575.0/48384.0  13525.0/55296.0	  277.0/14336.0	      0.25]
    #dc = np.array(c) - np.array(d) for error estimate
    dc = [c[0]-2825.0/27648.0,   0.0,            c[2]-18575.0/48384.0,
          c[3]-13525.0/55296.0, -277.00/14336.0, c[5]-0.25 ]

    h    = h0
    Emax = epsilon*max( norm(Y0), 1.0 )
    k    = np.zeros((6,8))
    k[0] = Y_prime(Y0,M,a)
    while 1 > 0 :
      k[1] = Y_prime(Y0 + h*(k[0]*b[1][0]), M, a)
      k[2] = Y_prime(Y0 + h*(k[0]*b[2][0]+k[1]*b[2][1]), M, a)
      k[3] = Y_prime(Y0 + h*(k[0]*b[3][0]+k[1]*b[3][1]+k[2]*b[3][2]), M, a)
      k[4] = Y_prime(Y0 + h*(k[0]*b[4][0]+k[1]*b[4][1]+k[2]*b[4][2]+k[3]*b[4][3]), M, a)
      k[5] = Y_prime(Y0 + h*(k[0]*b[5][0]+k[1]*b[5][1]+k[2]*b[5][2]+k[3]*b[5][3]+k[4]*b[5][4]), M, a)     
      # estimate the error at the current step
      E = norm(h*(k[0]*dc[0] + k[1]*dc[1] + k[2]*dc[2] + k[3]*dc[3] + k[4]*dc[4] + k[5]*dc[5]))

      if E < Emax:     # current step size h is good
         xi1 = xi0 + h
         Y1  = Y0  + h*(k[0]*c[0]+k[1]*c[1]+k[2]*c[2]+k[3]*c[3]+k[4]*c[4]+k[5]*c[5])
         if E > 0.0:
             h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)
         return [xi1,Y1,h]
      
      h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)

#--------------------------------------------
def rk45vb(xi0,Y0,h0,hmax,M,a,epsilon=1e-10):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        and adaptive stepsize control.
        h0 input step size, might be < 0, need be very careful,
        return [xi1, Y1, h1] (new stepsize) """

    # Cash-Karp parameters
    # a = [0.0, 0.2, 0.3, 0.6, 1.0, 0.875] not used, Y_prime(xi,Y) not depending on xi
    b = [[],
         [0.2],
         [0.075,        0.225 ],
         [0.3,            -0.9,        1.2      ],
         [-11.0/54.0,     2.5,         -70.0/27.0,     35.0/27.0],
         [1631.0/55296.0, 175.0/512.0, 575.0/13824.0,  44275.0/110592.0, 253.0/4096.0]]
    c  = [37.0/378.0,     0.0,         250.0/621.0,     125.0/594.0,      0.0,        512.0/1771.0]
    #d = [2825.0/27648.0  0.0	       18575.0/48384.0  13525.0/55296.0	  277.0/14336.0	      0.25]
    #dc = np.array(c) - np.array(d) for error estimate
    dc = [c[0]-2825.0/27648.0,   0.0,            c[2]-18575.0/48384.0,
          c[3]-13525.0/55296.0, -277.00/14336.0, c[5]-0.25 ]

    h    = h0
    Emax = epsilon*max( norm(Y0), 1.0 )
    k    = np.zeros( (6,len(Y0) ) )
    k[0] = Y_prime_12D(Y0,M,a)
    while 1 > 0 :
      k[1] = Y_prime_12D(Y0 + h*(k[0]*b[1][0]), M, a)
      k[2] = Y_prime_12D(Y0 + h*(k[0]*b[2][0]+k[1]*b[2][1]), M, a)
      k[3] = Y_prime_12D(Y0 + h*(k[0]*b[3][0]+k[1]*b[3][1]+k[2]*b[3][2]), M, a)
      k[4] = Y_prime_12D(Y0 + h*(k[0]*b[4][0]+k[1]*b[4][1]+k[2]*b[4][2]+k[3]*b[4][3]), M, a)
      k[5] = Y_prime_12D(Y0 + h*(k[0]*b[5][0]+k[1]*b[5][1]+k[2]*b[5][2]+k[3]*b[5][3]+k[4]*b[5][4]), M, a)
      # estimate the error at the current step
      E = norm(h*(k[0]*dc[0] + k[1]*dc[1] + k[2]*dc[2] + k[3]*dc[3] + k[4]*dc[4] + k[5]*dc[5]))

      if E < Emax:     # current step size h is good
         xi1 = xi0 + h
         Y1  = Y0  + h*(k[0]*c[0]+k[1]*c[1]+k[2]*c[2]+k[3]*c[3]+k[4]*c[4]+k[5]*c[5])
         if E > 0.0:
             h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)
         return [xi1,Y1,h]
      
      h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)

#------------------------------------------
def rk45vc(xi0,Y0,h0,hmax,M,a,epsilon=1e-10):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        and adaptive stepsize control.
        h0 input step size, might be < 0, need be very careful,
        return [xi1, Y1, h1] (new stepsize) """

    h    = h0
    Emax = epsilon*max( norm(Y0), 1.0 )
    k    = np.zeros( (6,len(Y0) ) )
    k[0] = Y_prime_12D(Y0,M,a)
    while 1 > 0 :
      k[1] = Y_prime_12D(Y0 + h*(k[0]*b_ck[1][0]), M, a)
      k[2] = Y_prime_12D(Y0 + h*(k[0]*b_ck[2][0]+k[1]*b_ck[2][1]), M, a)
      k[3] = Y_prime_12D(Y0 + h*(k[0]*b_ck[3][0]+k[1]*b_ck[3][1]+k[2]*b_ck[3][2]), M, a)
      k[4] = Y_prime_12D(Y0 + h*(k[0]*b_ck[4][0]+k[1]*b_ck[4][1]+k[2]*b_ck[4][2]+k[3]*b_ck[4][3]), M, a)
      k[5] = Y_prime_12D(Y0 + h*(k[0]*b_ck[5][0]+k[1]*b_ck[5][1]+k[2]*b_ck[5][2]+k[3]*b_ck[5][3]+k[4]*b_ck[5][4]), M, a)
      # estimate the error at the current step
      E = norm(h*(k[0]*dc_ck[0]+k[1]*dc_ck[1]+k[2]*dc_ck[2]+k[3]*dc_ck[3]+k[4]*dc_ck[4]+k[5]*dc_ck[5]))

      if E < Emax:     # current step size h is good
         xi1 = xi0 + h
         Y1  = Y0  + h*(k[0]*c_ck[0]+k[1]*c_ck[1]+k[2]*c_ck[2]+k[3]*c_ck[3]+k[4]*c_ck[4]+k[5]*c_ck[5])
         if E > 0.0:
             h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)
         return [xi1,Y1,h]
      
      h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)


#------------------------------------------
def rk45vd(xi0,Y0,h0,hmax,M,a,epsilon=1e-10):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        and adaptive stepsize control.
        h0 input step size, might be < 0, need be very careful,
        return [xi1, Y1, h1] (new stepsize) """

    h    = h0
    Emax = epsilon*max( norm(Y0), 1.0 )
    k    = np.zeros( (6,len(Y0) ) )
    k[0] = Y_prime(Y0,M,a)
    while 1 > 0 :
      k[1] = Y_prime(Y0 + h*(k[0]*b_ck[1][0]), M, a)
      k[2] = Y_prime(Y0 + h*(k[0]*b_ck[2][0]+k[1]*b_ck[2][1]), M, a)
      k[3] = Y_prime(Y0 + h*(k[0]*b_ck[3][0]+k[1]*b_ck[3][1]+k[2]*b_ck[3][2]), M, a)
      k[4] = Y_prime(Y0 + h*(k[0]*b_ck[4][0]+k[1]*b_ck[4][1]+k[2]*b_ck[4][2]+k[3]*b_ck[4][3]), M, a)
      k[5] = Y_prime(Y0 + h*(k[0]*b_ck[5][0]+k[1]*b_ck[5][1]+k[2]*b_ck[5][2]+k[3]*b_ck[5][3]+k[4]*b_ck[5][4]), M, a)
      # estimate the error at the current step
      E = norm(h*(k[0]*dc_ck[0]+k[1]*dc_ck[1]+k[2]*dc_ck[2]+k[3]*dc_ck[3]+k[4]*dc_ck[4]+k[5]*dc_ck[5]))

      if E < Emax:     # current step size h is good
         xi1 = xi0 + h
         Y1  = Y0  + h*(k[0]*c_ck[0]+k[1]*c_ck[1]+k[2]*c_ck[2]+k[3]*c_ck[3]+k[4]*c_ck[4]+k[5]*c_ck[5])
         if E > 0.0:
             h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)
         return [xi1,Y1,h]
      
      h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)



#------------------------------------------
def stepper(xi0,Y0,h0,M,a):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        NO adaptive stepsize control.
        h0 fixed input step size (might be < 0), return [xi1, Y1] """
    h    = h0
    k    = np.zeros((6,len(Y0)))
    k[0] = Y_prime(Y0,M,a)
    k[1] = Y_prime(Y0 + h*(k[0]*b_ck[1][0]), M, a)
    k[2] = Y_prime(Y0 + h*(k[0]*b_ck[2][0]+k[1]*b_ck[2][1]), M, a)
    k[3] = Y_prime(Y0 + h*(k[0]*b_ck[3][0]+k[1]*b_ck[3][1]+k[2]*b_ck[3][2]), M, a)
    k[4] = Y_prime(Y0 + h*(k[0]*b_ck[4][0]+k[1]*b_ck[4][1]+k[2]*b_ck[4][2]+k[3]*b_ck[4][3]), M, a)
    k[5] = Y_prime(Y0 + h*(k[0]*b_ck[5][0]+k[1]*b_ck[5][1]+k[2]*b_ck[5][2]+k[3]*b_ck[5][3]+k[4]*b_ck[5][4]), M, a)
    
    xi1 = xi0 + h
    Y1  = Y0  + h*(k[0]*c_ck[0]+k[1]*c_ck[1]+k[2]*c_ck[2]+k[3]*c_ck[3]+k[4]*c_ck[4]+k[5]*c_ck[5])
    return [xi1,Y1]

#------------------------------------------
def FPP(XI,Y,E_source,M,a):
    """ compute the Gravitionlly Faraday rotated polarization vector
    """
    n_point = len(XI)
    E_curr  = np.zeros(4)
    E_next  = np.zeros(4)
    E_curr[:] = E_source[:]
    for j in range(n_point-1):   # j= 0 : n_point-2
      d_xi  = XI[n_point-j-2] - XI[n_point-j-1]
      r     = Y[n_point-j-1, 1]
      theta = Y[n_point-j-1, 2]
      p_low = Y[n_point-j-1, 4:]
      g_up  = metric_up(r,theta,M,a)
      p_up  = np.dot(g_up, p_low)
      Gamma = connexion(M,a,r,theta)
      for k in range(4) :
          dE_k = 0.0
          for p in range(4):
              for q in range(4):
                  dE_k = dE_k + Gamma[k,p,q]*p_up[p]*E_curr[q]
          dE_k *= -d_xi
          E_next[k] = E_curr[k] + dE_k
      E_curr[:] = E_next[:]
    E_obs = E_curr
    return E_obs

#---------------------------------------
def FPP2(XI,Y,E_source,M,a):
    """ compute the Gravitionlly Faraday rotated polarization vector
    """
    n_point = len(XI)
    E_list  = np.zeros( (n_point,4) )
    E_list[0,:] = E_source[:]
    for j in range(n_point-1):   # j= 0 : n_point-2
      d_xi  = XI[n_point-j-2] - XI[n_point-j-1]
      r     = Y[n_point-j-1, 1]
      theta = Y[n_point-j-1, 2]
      p_low = Y[n_point-j-1, 4:]
      g_up  = metric_up(r,theta,M,a)
      p_up  = np.dot(g_up, p_low)
      Gamma = connexion(M,a,r,theta)
      for k in range(4) :
          dE_k = 0.0
          for p in range(4):
              for q in range(4):
                  dE_k = dE_k + Gamma[k,p,q]*p_up[p]*E_list[j,q]
          dE_k *= -d_xi
          E_list[j+1,k] = E_list[j,k] + dE_k
    E_obs = E_list[n_point-1,:]
    return E_list


