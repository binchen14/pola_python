#import numpy as np
#import scipy as sp
#import scipy.special as spsp
#from scipy.linalg      import norm
#from scipy.interpolate import interp1d
import pylab as pl
import time
from mod  import *
from mod3 import *
#-----------------------------------------
M,  r_obs    = [1.0,   1E6]
a,  inc_disk = [0.998, np.radians(75)]
r_disk,x_size,y_size = [20.0, 25.0, 25.0]
nx, ny      =  [50,   50]
Gamma, nr   =  [2.0, 3.0]

[ix,iy] = [40, 42]
#------------------------------------------
#   oneray(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny)

t_start   = time.time()
xi_list,Y_list,Q_list, xi_F_list,Y12D_F_list, EN_list, count, weight, delta, chi = oneray2(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny,ix,iy)
t_finish  = time.time()

r  = Y_list[:,1]        # backward r_list
rr = Y12D_F_list[:,1]   # forward  r_list
p2_list = pnorm2(Y_list,M,a)
E4_list = Y12D_F_list[:,8:]
x  = np.arange(Q_list.size)
dQ_list = (Q_list-Q_list[0])/Q_list[0]


run_time  = t_finish - t_start          # in seconds

print "run time in seconds is ", run_time
print "[numb_disk,numb_high,numb_hole,numb_infi,numb_over,numb_eqtr]="
print count

print Q_list
print "dQ list:", dQ_list
print "weight,delta,chi= ", weight, delta, chi


# 2-norm of the polarization vector, and the photon 4 vector
f1=pl.figure(1,figsize=(6.6,6.0),dpi=96)
ax= f1.gca()
ax.set_xscale('log')
#ax.set_yscale('symlog')
ax.set_xlim((1,1E6))
ax.set_ylim((-2.5E-7,1.5E-7))
ax.set_xlabel(r'r (in unit of $r_g$)',fontsize=18)
#ax.set_ylabel(r'($\times1E-7$)',fontsize=20)
ax.xaxis.set_tick_params(which='major',width=1.5,length=8)
ax.xaxis.set_tick_params(which='minor',width=1,length=5)
#ax.xaxis.set_ticks(np.logspace(0,6,61),minor=True)
#ax.xaxis.set_ticklabels(['$1$','$10^1$','$10^2$','$10^3$','$10^4$','$10^5$','$10^6$'],fontsize=16)
ax.yaxis.set_tick_params(which='major',width=1.5,length=8)
ax.yaxis.set_tick_params(which='minor',width=1,  length=5)
ax.yaxis.set_ticks(np.linspace(-2.5E-7, 1.5E-7, 41), minor=True)
ax.yaxis.set_ticklabels([-2.5, -2.0, -1.5, -1.0, -0.5,0.0, 0.5, 1.0,1.5],fontsize=14)
l1, = pl.plot(r, dQ_list,   linewidth=2,color='blue',    label=r'$\Delta{\cal C}/{\cal C}$')
l2, = pl.plot(r, p2_list,   linewidth=2,color='magenta', label=r'$p_ap^a$')
l3, = pl.plot(rr,EN_list-1, linewidth=2,color='red',     label=r'$E_aE^a-1$')
l1.set_dashes([2,4,8,4])
#l1.set_dashes([2, 4])
l2.set_dashes([8, 4])
#l3.set_dashes([16, 8])
ax.legend(loc='lower right',fontsize=14,handlelength=3.5,labelspacing=0.18, bbox_to_anchor = (0.9, 0.33))
ax.text(20, 1.E-7,r'Accuracy of KERTAP ($\times10^{-7}$)',fontsize=18)
#pl.savefig('./accuracy.eps')
pl.show()



f2=pl.figure(2)
ax= f2.gca()
ax.set_xscale('log')
#ax.set_yscale('log')
pl.plot(rr,E4_list[:,0],linewidth=2,label=r'$E_t$')
pl.plot(rr,E4_list[:,1],linewidth=2,label=r'$E_r$')
pl.plot(rr,E4_list[:,2],linewidth=2,label=r'$E_\theta$')
pl.plot(rr,E4_list[:,3],linewidth=2,label=r'$E_\phi$')
ax.legend(loc='lower right',fontsize=14,handlelength=3.5,labelspacing=0.2, bbox_to_anchor = (0.7, 0.025))
pl.show()





