from mod import *
#-----------------------------------------
def enorm2(Y_list,E4_list,M,a):
    """ compute g_abE^aE^b  """
    # E4_list forward, Y_list, backward.
    n  = len(E4_list) # return only length of the first dimension
    E2 = np.zeros(n)
    for i in range(n):
        r, theta = Y_list[n-i-1,1],Y_list[n-i-1,2]  # reverse Y_list
        g_low    = metric_low(r,theta,M,a)
        E4       = E4_list[i]
        E2[i]    = np.dot( np.dot(g_low, E4), E4)
    
    return E2

#-----------------------------------------
def enorm2vb(Y_list,M,a):
    """ compute g_abE^aE^b  """
    # Y_list is 12D, forward.
    n  = len(Y_list) # return numb of rows
    E2 = np.zeros(n)
    for i in range(n):
        r, theta = Y_list[i,1],Y_list[i,2]  # reverse Y_list
        g_low    = metric_low(r,theta,M,a)
        E4       = Y_list[i,8:]
        E2[i]    = np.dot( np.dot(g_low, E4), E4)
    
    return E2

#-----------------------------------------
def pnorm2(Y_list,M,a):
    """ compute g_abp^ap^b  """
    n  = len(Y_list)
    p2 = np.zeros(n)
    for i in range(n):
        r, theta = Y_list[i,1],Y_list[i,2]
        g_up     = metric_up(r,theta,M,a)
        p4       = Y_list[i,4:]
        p2[i]    = np.dot( np.dot(g_up, p4), p4)
    
    return p2

#---------------------------------------
def FPP4(xi_disk,Y8_disk,E4_source,h0,hmax,r_obs,M,a):
    """ polarization propagator using 12D rk45 method 
        return xi_list,Y_list,E2_list,E4_observer"""
    Y0 = np.concatenate((Y8_disk,E4_source))  # 12D starting point
    n_step_max   = 1024
    step_counter = 0
    xi, Y, h     = xi_disk, Y0, h0
    err_rel      = 1E-12
    horizon_soft = hsoft(M,a)
    
    xi_list = np.zeros( n_step_max + 1   )  # used by FPP
    Y_list  = np.zeros((n_step_max + 1, 12)) # used by FPP
    xi_list[0] = xi
    Y_list[0]  = Y
    while 1 > 0 :
        r_prev = Y[1]
        [xi1,Y1,h1]  = rk45ve(Y_prime_12D,xi,Y,h,hmax,M,a,err_rel)   # hmax nega
        step_counter += 1
        r_next = Y1[1]
        prod_cross = (r_prev - r_obs)*(r_next - r_obs)   # cross the r = r_obs sphere?
        
        if  step_counter > n_step_max:
            print "step_max error, quit"
            return
        if  r_next < horizon_soft:
            print "horizon problem,quit"
            return
        if prod_cross > 0:   # normal step forward
            [xi,Y,h] = [xi1,Y1,h1]
            xi_list[step_counter] = xi
            Y_list[step_counter]  = Y
        else:                # bisect to approca r_obs, must quit the while loop
            bsct_counter = 0        # bisect,find the h exactly hits the equatorial plane
            h_L, h_R     = [0, h]
            dr_L         = r_prev - r_obs
            while bsct_counter < 20 :
                h_mid = 0.5*(h_L + h_R)
                [xi_mid,Y_mid,h_new] = rk45ve(Y_prime_12D,xi,Y,h_mid,hmax,M,a,err_rel)
                dr_mid = Y_mid[1] - r_obs
                if np.abs(dr_mid/float(r_obs)) < 1E-6 :
                    [xi,Y] = [xi_mid,Y_mid]  # do not update h
                    xi_list[step_counter] = xi
                    Y_list[step_counter]  = Y
                    E2_list = enorm2vb(Y_list[:step_counter+1],M,a)
                    return xi_list[:step_counter+1],Y_list[:step_counter+1],E2_list,Y_list[step_counter,8:]
                prod_bsct = dr_L*dr_mid
                if prod_bsct < 0.0 :
                    h_R = h_mid
                else:
                    h_L = h_mid
                bsct_counter += 1
            print "bisect problem, quit"
            return

#-------------------------------------------------------------------
def oneray2(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny,ix,iy):
    """ % backward ray-tracing for a pencil beam of rays with rectangular cross sections
        % plus forward integration for the polarization after BRT
        % note:  consider higher order images, by change the max_num_cross parameter
        % note:  inc_disk should be in radian """

    max_numb_cross = 1         # maximum number of equatorial plane cross, higher order images
    n_step_max = 1024          # maxinum number of ray-tracing steps for runge-kutta
    err_rel    = 1E-12         # relative error for Runge-Kutta
    hmax       = 0.1*r_obs     # maximum stepsize of xi (affine para)
    r_max      = 1.5*r_obs     # stop those rays go to the other side of the disk
    hmax       = -hmax         #  < 0 !!!   flip the sign for backward ray-tracing

    horizon_soft = hsoft(M,a)                   # soften horizon a little bit.
    r_ISCO  = r_MS_plus(M,a)                    # r_ISCO=6/4.23/1.24M if a=0/0.5/0.998M
    dx_grid = x_size/float(nx)
    dy_grid = y_size/float(ny)
    
    # BRT statistics, numb_disk contains numb_high as a subset.
    [numb_disk,numb_high,numb_hole,numb_infi,numb_over,numb_eqtr]  = [0,0,0,0,0,0]
    
    [t0, r0, theta0, phi0] = [0.0, r_obs, inc_disk, 0.0]
    X0     = [t0, r0, theta0, phi0]     # same for all rays
    h0     = -r_obs/(512.0)             # initial step size < 0 !!!
    xi0    = 0.0
    for i_x in range(ix,ix+1):
        for i_y in range(iy, iy+1):
            # the up-left pixel is a ray pointing toward the lower-right corner
            p_r0     = r_obs                        # positive, for backward ray-tracing
            p_phi0   = dx_grid*(nx-i_x)             # phi increases when ix increases
            p_theta0 = dy_grid*(ny-i_y)             # y axis pointing downward (rows of matrix)
            p3  = np.array([p_r0,p_theta0,p_phi0])  # 3D un-normalized vector, numpy array needed
            p4_up  = p3_to_p4_up(p3,r0,theta0,M,a)
            print "p^a = ", p4_up[:]
            p4  = p3_to_p4(p3,r0,theta0,M,a)        # p4 is covariant 4-vector.
            Y0  = np.concatenate((X0,p4))           # initial 8D coordinates for ode45
            Q_carter0 = Carter(Y0,a)                # initial Carter constant
          
            step_counter = 0                        # reset for each ray
            numb_cross   = 0                        # times crossing the equatorial plane
            xi_list = np.zeros( n_step_max + 1   )  # used by FPP
            Y_list  = np.zeros((n_step_max + 1, 8)) # used by FPP
            Q_list  = np.zeros( n_step_max + 1   )
            EN_list = np.zeros( n_step_max + 1   )  # norm of E4_list
            xi_F_list   = np.zeros( n_step_max + 1 )
            Y12D_F_list = np.zeros((n_step_max + 1, 12))
            chi_obs = 0.0
            weight, delta = 0.0, 0.0
            
            xi,Y,h  = xi0,Y0,h0
            xi_list[0] = xi
            Y_list[0]  = Y
            Q_list[0]  = Q_carter0
            while 1 > 0 :                   # infinite ray-tracing loop
                cos_prev      = np.cos(Y[2])
                [xi1,Y1,h1]   = rk45ve(Y_prime,xi,Y,h,hmax,M,a,err_rel)
                cos_next      = np.cos(Y1[2])
                product_cos   = cos_prev*cos_next
                step_counter += 1
              
                if step_counter > n_step_max:
                    numb_over += 1      # too many ray-tracing steps
                    break
                if Y1[1] > r_max:
                    numb_infi += 1      # too far away from the BH
                    break
                if Y1[1] < horizon_soft:
                    numb_hole += 1
                    break
                if product_cos > 0.0  : #  a normal step forward
                    [xi,Y,h] = [xi1,Y1,h1]
                    xi_list[step_counter] = xi  # store the results for FPP
                    Y_list[step_counter]  = Y
                    Q_carter = Carter(Y,a)
                    Q_list[step_counter]  = Q_carter
                else:
                    numb_cross  += 1
                    bsct_counter = 0        # bisect,find the h exactly hits the equatorial plane
                    h_L, h_R     = [0, h]
                    cos_L        = cos_prev
                    while 1 > 0 :
                        h_mid = 0.5*(h_L + h_R)
                        #[xi_mid,Y_mid] = stepper(xi,Y,h_mid,M,a)
                        [xi_mid,Y_mid,h_new] = rk45ve(Y_prime,xi,Y,h_mid,hmax,M,a,err_rel) # hmax<0
                        cos_mid = np.cos(Y_mid[2])
                        if np.abs(cos_mid) < 1E-6 or bsct_counter == 20 :
                            [xi,Y,h] = [xi_mid,Y_mid,h_new]
                            #[xi,Y] = [xi_mid,Y_mid]  # do not update h
                            break
                        prod_bsct = cos_L*cos_mid
                        if prod_bsct < 0.0 :
                            h_R = h_mid
                        else:
                            h_L = h_mid
                        bsct_counter += 1
                    if  np.cos(Y[2]) > 1E-6:
                        print "something wrong at (i_x,i_y) ", i_x, i_y
                        break
                    if  Y[1] >= r_ISCO and Y[1] <= r_disk:
                        numb_disk += 1
                        if numb_cross > 1:
                            numb_high += 1
                        xi_list[step_counter] = xi
                        Y_list[step_counter]  = Y
                        Q_carter = Carter(Y,a)
                        Q_list[step_counter]  = Q_carter
                        if Q_carter0 < 1E-8:
                            err_Q = np.abs(Q_carter - Q_carter0)
                        else:
                            err_Q = np.abs((Q_carter - Q_carter0)/Q_carter0)
                        if err_Q > 1E-4:
                            print "Q bad at (ix,iy) ",i_x, " ",i_y," Q0= ", Q_carter0, "num_cross", numb_cross
                        
                        [weight, delta, E4_source] = polarization(Y,M,a)
                        #xi_short,Y_short = xi_list[0:step_counter+1], Y_list[0:step_counter+1,:]
                        xi_F_list,Y12D_F_list,EN_list,E4_obs = FPP4(xi,Y,E4_source,-h,-hmax,r_obs,M,a)
                        #E4_list = FPP3(xi_short, Y_short, E4_source, M, a)
                        #EN_list = enorm2(Y_short,E4_list,M,a)
                        chi_obs = polar_obs(X0,E4_obs,M,a)
                        break
                    elif  numb_cross >=  max_numb_cross :
                        numb_eqtr += 1
                        break
                    else:
                        [xi,Y,h] = [xi1,Y1,h1] # do not use the bisect results
                        xi_list[step_counter] = xi
                        Y_list[step_counter]  = Y
                        Q_carter = Carter(Y,a)
                        Q_list[step_counter]  = Q_carter


    count = np.array([numb_disk,numb_high,numb_hole,numb_infi,numb_over,numb_eqtr])

    return xi_list[:step_counter+1],Y_list[:step_counter+1],Q_list[:step_counter+1],xi_F_list,Y12D_F_list,EN_list,count, weight, delta, chi_obs

#--------------------------------------

