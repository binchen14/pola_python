import numpy as np
import scipy as sp
import scipy.special as spsp
from scipy.linalg      import norm
from scipy.interpolate import interp1d

#-----------------------------------------------
Mu_list =  np.linspace(0.0,1.0,21)
I_list  =  [0.41441,0.47490,0.52397,0.57001,0.61439,0.65770,
            0.70029,0.74234,0.78398,0.82530,0.86637,0.90722,
            0.94789,0.98842,1.02882,1.06911,1.10931,1.14943,
            1.18947,1.22945,1.26938]
del_list = [0.11713,0.08979,0.07448,0.06311,0.05410,0.04667,
            0.04041,0.03502,0.03033,0.02619,0.02252,0.01923,
            0.01627,0.01358,0.011123,0.008880,0.006818,0.004919,
            0.003155,0.001522,0]    
f1 = interp1d(Mu_list, I_list) 	 # kind='cubic') ---> weight
f2 = interp1d(Mu_list, del_list) # kind='cubic') ---> delta

#-------------------------------------------------
# Cash-Karp parameters
# a_ck = [0.0, 0.2, 0.3, 0.6, 1.0, 0.875] not used, Y_prime(xi,Y) not depending on xi

b_ck =np.array([[0.0,            0.0,         0.0,            0.0,              0.0],
                [0.2,            0.0,         0.0,            0.0,              0.0],
                [0.075,          0.225,       0.0,            0.0,              0.0],
                [0.3,            -0.9,        1.2,            0.0,              0.0],
                [-11.0/54.0,     2.5,         -70.0/27.0,     35.0/27.0,        0.0],
                [1631.0/55296.0, 175.0/512.0, 575.0/13824.0,  44275.0/110592.0, 253.0/4096.0]])
c_ck  = np.array([37.0/378.0,   0.0,   250.0/621.0,     125.0/594.0,      0.0,    512.0/1771.0])
#d_ck  = [2825.0/27648.0  0.0	       18575.0/48384.0  13525.0/55296.0	  277.0/14336.0	      0.25]
#dc_ck = np.array(c_ck) - np.array(d_ck) for error estimate
dc_ck = np.array([c_ck[0]-2825.0/27648.0,   0.0,            c_ck[2]-18575.0/48384.0,
                  c_ck[3]-13525.0/55296.0, -277.00/14336.0, c_ck[5]-0.25 ])

#--------------------------------------------------
# compute a few important terms in kerr metric
def kerr(r,theta,M,a):
    """ return np.array, [delta, rho2, alpha, omega] in Kerr metric """
    sin_theta = np.sin(theta)
    cos_theta = np.cos(theta)
    sin2      = sin_theta*sin_theta
    cos2      = cos_theta*cos_theta
    r2, a2 	  = r*r, a*a
    tmr       = 2.0*M*r
    
    rho2      = r2 + a2*cos2
    Delta     = r2 - tmr + a2
    Sig2      = rho2*(r2 + a2) + tmr*a2*sin2
    alpha2    = rho2*Delta/Sig2
    omega     = a*tmr/Sig2

    return   np.array([Delta,rho2,alpha2,omega])
#------------------------------------------------
def r_MS_plus(M,a):
    """ compute the innermost stable circular orbit for prograde orbit """
    s  = a/M
    s2 = s*s
    Z1 = 1.0 + spsp.cbrt(1.0 - s2)*( spsp.cbrt(1.0 + s) + spsp.cbrt(1.0 - s) )
    Z2 = np.sqrt(3.0*s2 + Z1*Z1)
    
    r_ISCO = M*(3.0 + Z2 - np.sqrt((3.0 - Z1)*(3.0 + Z1 + 2.0*Z2)) )
    
    return r_ISCO
#----------------------------------------------
def hsoft(M,a,del_h=0.001):
    """ outer horizon, with a soften parameter """
    r_plus = M + np.sqrt(M*M - a*a)
    
    return r_plus*(1.0 + del_h)
#-----------------------------------------------
def Carter(Y,a):
    """ % Carter's constant for massless particles
        % note that Y[5:8] is the covariant 4-momentum vector, say p_a, not p^a.
    """
    [theta, p_t, p_theta, p_phi] = Y[2], Y[4], Y[6], Y[7]
    [cos1, sin1] = [np.cos(theta),np.sin(theta)]
    cos2  = cos1*cos1
    sin2  = sin1*sin1
    
    Q_carter = p_theta*p_theta + cos2*(p_phi*p_phi/sin2 - a*a*p_t*p_t)

    return Q_carter

#-----------------------------------------------
def energy_ZAMO(r,theta,M,a,p_low):
    """ % energy of an photon as measured by a zero-angular-momentum observer
        %    u^a = (1/alpha, 0, 0, omega/alpha), u_a = (-\alpha, 0, 0, 0), 
        %    u_3 = 0, therefore, zero-angular-momentum """
    
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a)
    s1 = 1.0/np.sqrt(np.abs(alpha2))  # s1 = 1/alpha
    E_obs = s1*(p_low[0] + omega*p_low[3])
    
    return E_obs

#-----------------------------------------------
def metric_low(r,theta,M,a):
    """ % covariant metric tensor for Kerr in Boyer-Lindquist with (-,+,+,+) signature
        % where g_low is a 4x4 np.array, with index  0-3 (t,r,theta,phi) """
    
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a)
    sin1   = np.sin(theta)
    sin2   = sin1*sin1
    a2,r2  = a*a, r*r
    Sig2   = rho2*(r2 + a2) + 2.0*M*r*a2*sin2
    s1     = 2.0*M*r/rho2
    
    g_low      = np.zeros((4,4))
    g_low[0,0] = -(1.0 - s1)
    g_low[1,1] = rho2/Delta
    g_low[2,2] = rho2
    g_low[3,3] = Sig2*sin2/rho2
    
    g_low[0,3] = -s1*a*sin2
    g_low[3,0] =  g_low[0,3]

    return g_low

#---------------------------------------------------
def metric_up(r,theta,M,a):
    """ % contravariant metric tensor for Kerr metric
        % where g_up is a 4x4 np.array """
    
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a) 
    sin1    = np.sin(theta)
    sin2    = sin1*sin1
    a2, r2  = a*a, r*r
    
    s1 = 2.0*M*r/rho2

    g_up      =  np.zeros((4,4))
    g_up[0,0] = -1.0/alpha2
    g_up[1,1] =  Delta/rho2
    g_up[2,2] =  1.0/rho2
    g_up[3,3] =  (Delta - a2*sin2)/(rho2*Delta*sin2)
    
    g_up[0,3] = -s1*a/Delta
    g_up[3,0] =  g_up[0,3]

    return g_up
#----------------------------------------------------
def energy_CO_plus(Y,M,a):
    """photon energy w.r.t. comoving observer in the Keplerian flow.
    """
    r, theta = Y[1], Y[2]
    g_low    = metric_low(r,theta,M,a)
    Omega    = np.sqrt(M)/(r**1.5 + a*np.sqrt(M))   # Kepelerian angluar velocity
    s1       = g_low[0,0] + 2.0*g_low[0,3]*Omega + g_low[3,3]*Omega*Omega
    gamma    = np.sqrt(np.abs(-1.0/s1))      # U^a = gamma*(1,0,0,Omega)
    E_disk   = gamma*(Y[4] + Omega*Y[7])     # U^a*p_a

    return E_disk

#----------------------------------------------------
def connexion(M,a,r,theta):
    """ affine connections, Gamma^i_{j,k} for Kerr metric
        return np.array """
    
    Gamma = np.zeros((4,4,4))

    a2,   r2    = a*a, r*r
    cos1, sin1  = np.cos(theta), np.sin(theta)
    cos2, sin2  = cos1*cos1,     sin1*sin1
    a3,   sin3  = a2*a,          sin2*sin1

    sind  = 2.0*sin1*cos1
    cot1  = cos1/sin1        	# 1.0/np.tan(theta) 
    rho2  = r2 + a2*cos2
    rho4  = rho2*rho2
    rho6  = rho4*rho2
    Delta = r2 - 2.0*M*r + a2
    w2    = r2 - a2*cos2
    w3    = Delta*rho4   
 
    Gamma[0,1,0] = M*(r2 + a2)*w2/w3
    Gamma[0,0,1] = Gamma[0,1,0]
    Gamma[0,2,0] = -a2*M*r*sind/rho4
    Gamma[0,0,2] = Gamma[0,2,0]
    Gamma[0,3,1] = -a*M*sin2*( rho2*(r2-a2) + 2.0*r2*(r2+a2) )/w3
    Gamma[0,1,3] = Gamma[0,3,1]
    Gamma[0,3,2] = 2.0*a3*M*r*cos1*sin3/rho4
    Gamma[0,2,3] = Gamma[0,3,2]
    Gamma[1,0,0] = M*Delta*w2/rho6
    Gamma[1,1,1] = (r*a2*sin2 - M*w2)/(Delta*rho2)
    Gamma[1,2,1] = -a2*sind/(2.0*rho2)
    Gamma[1,1,2] = Gamma[1,2,1]
    Gamma[1,2,2] = -r*Delta/rho2
    Gamma[1,3,0] = -a*M*Delta*w2*sin2/rho6
    Gamma[1,0,3] = Gamma[1,3,0]
    Gamma[1,3,3] = -Delta*sin2*(r*rho4-M*a2*sin2*w2)/rho6
    Gamma[2,0,0] = -M*a2*r*sind/rho6
    Gamma[2,1,1] = a2*sind/(2.0*Delta*rho2)
    Gamma[2,2,1] = r/rho2
    Gamma[2,1,2] = Gamma[2,2,1]
    Gamma[2,2,2] = -a2*sind/(2.0*rho2)
    Gamma[2,3,0] = a*M*r*(r2+a2)*sind/rho6
    Gamma[2,0,3] = Gamma[2,3,0]
    Gamma[2,3,3] = -sind*(rho4*(r2+a2)+2.0*M*r*a2*sin2*(2.0*rho2+a2*sin2))/(2.0*rho6)
    Gamma[3,1,0] = a*M*w2/w3
    Gamma[3,0,1] = Gamma[3,1,0]
    Gamma[3,2,0] = -2.0*a*M*r*cot1/rho4
    Gamma[3,0,2] = Gamma[3,2,0]
    Gamma[3,3,1] = (r*rho4 - 2.0*M*r2*rho2 - M*a2*sin2*w2)/w3
    Gamma[3,1,3] = Gamma[3,3,1]
    Gamma[3,3,2] = (rho4 + 2.0*M*r*a2*sin2)*cot1/rho4
    Gamma[3,2,3] = Gamma[3,3,2]
    
    return Gamma
#----------------------------------------------------

def p3_to_p4(p3,r,theta,M,a):
    """%   convert 3D vector (w.r.t. orthonormal triad, e_r, e_theat, e_phi)
       %   to 4-D covariant momentum vector, p4_low, w.r.t. Kerr coordinate basis.
       %   p3 must be numpy array, return np.array """
        
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a) 
    g_low = metric_low(r,theta,M,a)
     
    [p_r, p_theta, p_phi] = p3/norm(p3) 
    alpha   = np.sqrt(np.abs(alpha2))
        
    p_up    = np.zeros(4)    
    p_up[0] = 1.0/alpha
    p_up[1] =                 p_r/np.sqrt(g_low[1,1])
    p_up[2] =             p_theta/np.sqrt(g_low[2,2])
    p_up[3] = omega/alpha + p_phi/np.sqrt(g_low[3,3])
    
    p4_low  = np.dot(g_low,p_up)   # contravariant to covariant 
    
    return p4_low

#-----------------------------
def p3_to_p4_up(p3,r,theta,M,a):
    """%   convert 3D vector (w.r.t. orthonormal triad, e_r, e_theat, e_phi)
       %   to 4-D covariant momentum vector, p4_low, w.r.t. Kerr coordinate basis.
       %   p3 must be numpy array, return np.array """
        
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a) 
    g_low = metric_low(r,theta,M,a)
     
    [p_r, p_theta, p_phi] = p3/norm(p3) 
    alpha   = np.sqrt(np.abs(alpha2))
        
    p_up    = np.zeros(4)    
    p_up[0] = 1.0/alpha
    p_up[1] =                 p_r/np.sqrt(g_low[1,1])
    p_up[2] =             p_theta/np.sqrt(g_low[2,2])
    p_up[3] = omega/alpha + p_phi/np.sqrt(g_low[3,3])
     
    return p_up

#----------------------------------------------------
def Y_prime(Y,M,a):
    """ compute Y'(xi,Y), Y the 8-D co-ordinates (x^a,p_b), np.array
     xi, the affine parameter, not included, return np.array """

    DY      = np.zeros(8)
    [t,r,theta,phi,p_t,p_r,p_theta,p_phi] = Y        
    
    sin_theta  = np.sin(theta)
    cos_theta  = np.cos(theta)
    sin_cos    = sin_theta*cos_theta
    sin2       = sin_theta*sin_theta
    
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a) 
    
    Delta2 = Delta*Delta
    rho4   = rho2*rho2
    a2,r2  = a*a, r*r
    
    s1   =  2.0*M*a*r/(rho2*Delta)
    s2   =  (Delta - a2*sin2)/(rho2*Delta*sin2)
    
    DY[0] = -p_t/alpha2 - s1*p_phi
    DY[1] = Delta*p_r/rho2
    DY[2] = p_theta/rho2
    DY[3] = s2*p_phi - s1*p_t
     
    s3 = 2.0*M/(Delta*rho2)
    s4 = (a2 + r2)*(a2 - r2)/Delta 
    s5 = 2.0*r2*a2*sin2/rho2
    s6 = 4.0*M*a2*r*(a2 + r2)/(Delta*rho4)    
    c1 = s3*(s4 - s5) 
    d1 = s6*sin_cos
    
    s7  = r - M - r*Delta/rho2
    c3  = 2.0*s7/rho2
    d3  = 2.0*a2*Delta*sin_cos/rho4
    
    c4  = -2.0*r/rho4
    d4  =  2.0*a2*sin_cos/rho4
    
    s8  = rho2*(a2 - r2) - 2.0*r2*Delta
    c2  =  2.0*M*a*s8/(rho4*Delta2)    
    d2  =  4.0*M*r*(a2*a)*sin_cos/(rho4*Delta)
    
    s9  = 2.0*a2*sin2*( r*Delta + (r - M)*rho2 ) - 2.0*r*Delta2
    s10 = rho2*(rho2 - 2.0*M*r) + 2.0*M*r*a2*sin2    
    c5  = s9/(rho4*Delta2*sin2)
    d5  = -2.0*cos_theta*s10/(rho4*Delta*sin_theta*sin2)
     
    DY[4] = 0.0 
    DY[7] = 0.0
 
    DY[5] = ( 0.5*c1*p_t*p_t + c2*p_t*p_phi - 0.5*c3*p_r*p_r 
            - 0.5*c4*p_theta*p_theta - 0.5*c5*p_phi*p_phi ) 
    
    DY[6] = ( 0.5*d1*p_t*p_t + d2*p_t*p_phi - 0.5*d3*p_r*p_r 
            - 0.5*d4*p_theta*p_theta - 0.5*d5*p_phi*p_phi )         

    return DY
#------------------------------------------
def Y_prime_12D(Y,M,a):
    """ compute Y'(xi,Y), Y the 12-D co-ordinates (x^a,p_b,E^c), np.array
        xi, the affine parameter, not included, return np.array """
    DY          = np.zeros(12)
    DY[0:8]     = Y_prime(Y[:8],M,a)
    r,  theta   = Y[1],Y[2]
    p4, E4      = Y[4:8], Y[8:]            # p_a, E^c
    g_up        = metric_up(r,theta,M,a)
    p_up        = np.dot(g_up, p4)
    Gamma       = connexion(M,a,r,theta)   # compute the Christofel symbols
    
    for i in range(4):
        for j in range(4):
            for k in range(4):
                DY[i+8] += Gamma[i,j,k]*p_up[j]*E4[k]
        DY[i+8] *= -1                       # now take care of the minus sign

    return DY

#------------------------------------------
def rk45ve(f,xi0,Y0,h0,hmax,M,a,epsilon=1e-10):
    """ moving ONE step forward using rk45 (Cash-Karp method),
        and adaptive stepsize control.
        h0 input step size, might be < 0, need be very careful,
        return [xi1, Y1, h1] (new stepsize) """

    h    = h0
    Emax = epsilon*max( norm(Y0), 1.0 )
    k    = np.zeros( (6,len(Y0) ) )
    k[0] = f(Y0,M,a)
    while 1 > 0 :
      k[1] = f(Y0 + h*(k[0]*b_ck[1][0]), M, a)
      k[2] = f(Y0 + h*(k[0]*b_ck[2][0]+k[1]*b_ck[2][1]), M, a)
      k[3] = f(Y0 + h*(k[0]*b_ck[3][0]+k[1]*b_ck[3][1]+k[2]*b_ck[3][2]), M, a)
      k[4] = f(Y0 + h*(k[0]*b_ck[4][0]+k[1]*b_ck[4][1]+k[2]*b_ck[4][2]+k[3]*b_ck[4][3]), M, a)
      k[5] = f(Y0 + h*(k[0]*b_ck[5][0]+k[1]*b_ck[5][1]+k[2]*b_ck[5][2]+k[3]*b_ck[5][3]+k[4]*b_ck[5][4]), M, a)
      # estimate the error at the current step
      E = norm(h*(k[0]*dc_ck[0]+k[1]*dc_ck[1]+k[2]*dc_ck[2]+k[3]*dc_ck[3]+k[4]*dc_ck[4]+k[5]*dc_ck[5]))

      if E < Emax:     # current step size h is good
         xi1 = xi0 + h
         Y1  = Y0  + h*(k[0]*c_ck[0]+k[1]*c_ck[1]+k[2]*c_ck[2]+k[3]*c_ck[3]+k[4]*c_ck[4]+k[5]*c_ck[5])
         if E > 0.0:
             h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)
         return [xi1,Y1,h]
      
      h = min(hmax, 0.85*h*(Emax/E)**0.2) if hmax > 0 else max(hmax, 0.85*h*(Emax/E)**0.2)

#---------------------------------------
def polar_obs(X0,E_obs,M,a):
    """ compute the polarization angle at the observer
        chi is in radian, measured w.r.t. the e_phi direction at the observer
        assume E_obs a 4D COLUMN vector, X0 a 4D ROW vector """
    s1        = 1E-60         	#  soften the denominator avoid zeros
    r         = X0[1]
    theta     = X0[2]
    sin_theta = np.sin(theta)
    cos_theta = np.cos(theta)
    sin2      = sin_theta*sin_theta
    cos2      = cos_theta*cos_theta
    r2        = r*r
    a2        = a*a
    
    rho2      = r2 + a2*cos2
    # Delta     = r2 - 2.0*M*r + a2
    Sig2      = rho2*(r2 + a2) + 2.0*M*r*a2*sin2
    # alpha2    = rho2*Delta/Sig2
    omega     = 2.0*a*M*r/Sig2

    # g11       = rho2/Delta
    g22       = rho2    
    g33       = Sig2*sin2/rho2

    #alpha     = np.sqrt(alpha2)    
    #M_zamo_coord_to_hat =[[       alpha,            0,             0,          0       ],
    #     		  [           0,        np.sqrt(g11),      0,          0       ],
    #     		  [           0,            0,     np.sqrt(g22),       0       ],
    #     		  [  -np.sqrt(g33)*omega,   0,             0,     np.sqrt(g33) ] ]
    #E_obs_hat    = np.dot(M_zamo_coord_to_hat, E_obs)  
    #E_hat_theta  = E_obs_hat[2]    # % point downward
    #E_hat_phi    = E_obs_hat[3]    # % point to the right

    E_hat_theta  = np.sqrt(g22)*E_obs[2]
    E_hat_phi    = np.sqrt(g33)*(-omega*E_obs[0] + E_obs[3])
    chi          = 0.5*np.arctan( -E_hat_theta/(E_hat_phi + s1) )
    return chi
#-------------------------------------------
def polarization(Y,M,a):
    """ compute the initial polarization vector E on the accretion disk
	%  + given photon's 8D phase-space coordinates Y(1:8)
	%  E is a 4-row column vector 
	%  delta is the degree of polarization; weight is the limbdarkening factor
	%  the table used of interpolation comes from Chandra1960 Tab XXIV
    """
    r     = Y[1]
    theta = Y[2]
    p_low = Y[4:]
    g_low = metric_low(r,theta,M,a)
    g_up  = metric_up(r,theta,M,a)    
    p_up  = np.dot(g_up,p_low)         
    
    Omega_K = np.sqrt(M)/( r**1.5 + a*np.sqrt(M) )  # Kepelerian angluar velocity
    
    g11    = g_low[0,0]  # note the shift of index
    g14    = g_low[0,3]
    g44    = g_low[3,3]
    g22    = g_low[1,1]
    g33    = g_low[2,2]
    
    s1      = g11 + 2.0*g14*Omega_K + g44*Omega_K*Omega_K
    gamma   = np.sqrt(np.abs(-1.0/s1))         #  U^a = gamma*(1,0,0,Omega)
    lambda0 = -(g14 + g44*Omega_K)/(g11 + g14*Omega_K)
    tau     = 1.0/np.sqrt(g11*lambda0*lambda0 + 2.0*g14*lambda0 + g44)
    
    s2      = 1.0 - lambda0*Omega_K
    
    M_coord_to_hat = [  [ 1.0/(gamma*s2),         0,              0, -lambda0/(gamma*s2) ],
         		[           0,  np.sqrt(g22),              0,       0      ],
         		[           0,             0,   -np.sqrt(g33),      0      ],
         		[-Omega_K/(tau*s2),        0,              0,  1.0/(tau*s2) ]]   
    
    p_hat  = np.dot(M_coord_to_hat,p_up)   # with respect to the tetrad
    
    norm3  = norm(p_hat[1:]) 	 	   # sqrt( p_hat(1)^2 + p_hat(2)^2 + p_hat(3)^2 )
    mu     = np.abs(p_hat[2])/norm3        #  % the upward normal to equatororial
    
    s3     = 1.0/np.sqrt( p_hat[1]*p_hat[1] + p_hat[3]*p_hat[3] )
    E_hat  = s3*np.array( [ 0, -p_hat[3], 0, p_hat[1] ] )
    
    M_hat_to_coord = [[ gamma,              0,              0,         tau*lambda0],
                      [   0,           1.0/np.sqrt(g22),    0,               0   ],
                      [   0,                0,           -1.0/np.sqrt(g33),  0   ],
                      [ gamma*Omega_K,      0,              0,               tau ] ] 
     
    E       = np.dot(M_hat_to_coord,E_hat)
    
    return [ f1(mu), f2(mu), E ]

#---------------------------------------
def FPP5(xi_disk,Y8_disk,E4_source,h0,hmax,r_obs,M,a):
    """ polarization propagator using 12D rk45ve() method
        h0, and hmax > 0; return E^a_{observer} """

    err_rel      = 1E-11
    n_step_max   = 1024
    horizon_soft = hsoft(M,a)

    Y0 = np.concatenate((Y8_disk,E4_source))  # 12D starting point
    xi, Y, h     = xi_disk, Y0, h0
    step_counter = 0
    
    while 1 > 0 :     # infinite ray-tracing loop
        r_prev = Y[1]
        [xi1,Y1,h1]  = rk45ve(Y_prime_12D,xi,Y,h,hmax,M,a,err_rel)   # hmax nega
        step_counter += 1
        r_next = Y1[1]
        prod_cross = (r_prev - r_obs)*(r_next - r_obs)   # cross the r = r_obs sphere?
        
        if step_counter > n_step_max or r_next < horizon_soft :
            print "step_max or horizon error, quit"
            return
        if prod_cross > 0:   # normal step forward
            [xi,Y,h] = [xi1,Y1,h1]
        else:               #  first, bisect to approcah r_obs, then quit the while loop
            bsct_counter = 0        # bisect,find the h exactly hits the equatorial plane
            h_L, h_R     = [0, h]
            dr_L         = r_prev - r_obs
            while bsct_counter < 20 :
                h_mid = 0.5*(h_L + h_R)
                [xi_mid,Y_mid,h_new] = rk45ve(Y_prime_12D,xi,Y,h_mid,hmax,M,a,err_rel)
                dr_mid = Y_mid[1] - r_obs
                if np.abs(dr_mid/float(r_obs)) < 1E-6 :  # Y_mid good to go
                    return Y_mid[8:]                    # return E^a_observer
                prod_bsct = dr_L*dr_mid
                if prod_bsct < 0.0 :
                    h_R = h_mid
                else:
                    h_L = h_mid
                bsct_counter += 1
            print "bisect problem, quit"
            return
#-------------------------------------------------------------------
def BRTP(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny):
    """ % backward ray-tracing for a pencil beam of rays with rectangular cross sections
        % plus forward integration for the polarization after BRT
        % note:  consider higher order images, by change the max_num_cross parameter
        % note:  inc_disk should be in radian """

    err_rel    = 1E-11         # relative error for Runge-Kutta
    max_numb_cross = 1         # maximum number of equatorial plane cross, higher order images
    n_step_max = 1024          # maxinum number of ray-tracing steps for runge-kutta
    hmax       = 0.1*r_obs     # maximum stepsize of xi (affine para)
    r_max      = 1.5*r_obs     # stop those rays go to the other side of the disk
    hmax_neg   = -hmax         #  < 0 !!!   flip the sign for backward ray-tracing

    horizon_soft = hsoft(M,a)                   # soften horizon a little bit.
    r_ISCO  = r_MS_plus(M,a)                    # r_ISCO=6/4.23/1.24M if a=0/0.5/0.998M
    dx_grid = x_size/float(nx)
    dy_grid = y_size/float(ny)
    
    Image_P = np.zeros((2*ny+1, 2*nx+1, 3))     # [weight, delta, chi] (radian)
    Image_Q = np.zeros((2*ny+1, 2*nx+1   ))     # relative error in Carter
    Image_G = np.zeros((2*ny+1, 2*nx+1   ))     # E_observer/E_emitter
    Image_Y = np.zeros((2*ny+1, 2*nx+1, 8))     # (x^a,p_a)

    # BRT statistics, numb_disk contains numb_high as a subset.
    [numb_disk,numb_high,numb_hole,numb_infi,numb_over,numb_eqtr]  = [0,0,0,0,0,0]
    
    [t0, r0, theta0, phi0] = [0.0, r_obs, inc_disk, 0.0]
    X0     = [t0, r0, theta0, phi0]     # same for all rays
    h0     = -r_obs/(512.0)             # initial step size < 0 !!!
    xi0    = 0.0
    dQ_min, dQ_max = [10.0, 0.0]
    for i_x in range(2*nx + 1):
        for i_y in range(2*ny + 1):
            # the up-left pixel is a ray pointing toward the lower-right corner
            p_r0     = r_obs                        # positive, for backward ray-tracing
            p_phi0   = dx_grid*(nx-i_x)             # phi increases when ix increases
            p_theta0 = dy_grid*(ny-i_y)             # y axis pointing downward (rows of matrix)
            p3  = np.array([p_r0,p_theta0,p_phi0])  # 3D un-normalized vector, numpy array needed
            p4  = p3_to_p4(p3,r0,theta0,M,a)        # p4 is covariant 4-vector.
            Y0  = np.concatenate((X0,p4))           # initial 8D coordinates for ode45
            Q_carter0 = Carter(Y0,a)                # initial Carter constant
            
            step_counter = 0                        # reset for each ray
            numb_cross   = 0                        # times crossing the equatorial plane
            xi,Y,h  = xi0,Y0,h0             # [xi,Y,h] will be dynamically updated
            while 1 > 0 :                   # infinite ray-tracing loop
                cos_prev      = np.cos(Y[2])
                [xi1,Y1,h1]   = rk45ve(Y_prime,xi,Y,h,hmax_neg,M,a,err_rel)
                cos_next      = np.cos(Y1[2])
                product_cos   = cos_prev*cos_next
                step_counter += 1
              
                if step_counter > n_step_max:
                    numb_over += 1      # too many ray-tracing steps
                    break
                if Y1[1] > r_max:
                    numb_infi += 1      # too far away from the BH
                    break
                if Y1[1] < horizon_soft:
                    numb_hole += 1
                    break
                if product_cos > 0.0  : #  a normal step forward
                    [xi,Y,h] = [xi1,Y1,h1]
                else:
                    numb_cross  += 1
                    
                    bsct_counter = 0        # bisect,find the h exactly hits the equatorial plane
                    h_L, h_R     = [0, h]
                    cos_L        = cos_prev
                    while 1 > 0 :
                        h_mid = 0.5*(h_L + h_R)
                        #[xi_mid,Y_mid] = stepper(xi,Y,h_mid,M,a)
                        [xi_mid,Y_mid,h_new] = rk45ve(Y_prime,xi,Y,h_mid,hmax_neg,M,a,err_rel)
                        cos_mid = np.cos(Y_mid[2])
                        if np.abs(cos_mid) < 1E-6 or bsct_counter == 20 :
                            [xi,Y,h] = [xi_mid,Y_mid,h_new]
                            #[xi,Y] = [xi_mid,Y_mid]  # do not update h
                            break
                        prod_bsct = cos_L*cos_mid
                        if prod_bsct < 0.0 :
                            h_R = h_mid
                        else:
                            h_L = h_mid
                        bsct_counter += 1
                    if  np.cos(Y[2]) > 1E-6:
                        print "bisecting problem at (i_x,i_y) ", i_x, i_y
                        break
                    # [xi,Y] now punches on the equatorial plane
                    if  Y[1] >= r_ISCO and Y[1] <= r_disk:
                        numb_disk += 1
                        if numb_cross > 1:
                            numb_high += 1
                        Q_carter = Carter(Y,a)
                        err_Q = np.abs((Q_carter - Q_carter0)/Q_carter0)
                        dQ_min, dQ_max = [min(dQ_min,err_Q), max(dQ_max,err_Q)]
                        E_obs    = energy_ZAMO(r0,theta0,M,a,p4) # observed energy
                        E_disk   = energy_CO_plus(Y,M,a)
                        redshift = E_obs/E_disk
                        Image_G[i_y,i_x]   = redshift
                        Image_Y[i_y,i_x,:] = Y          # this finishes the backward part.
                                                          
                        [weight, delta, E4_source] = polarization(Y,M,a)
                        E4_obs = FPP5(xi,Y,E4_source,-h,hmax,r_obs,M,a)   # h<0, hmax > 0.
                        chi    = polar_obs(X0,E4_obs,M,a)
                        Image_P[i_y,i_x,:] = [weight,delta,chi]
                        break
                    elif  numb_cross >=  max_numb_cross :
                        numb_eqtr += 1
                        break
                    else:
                        [xi,Y,h] = [xi1,Y1,h1] # do not use the bisect results
    dQ = np.array([dQ_min,dQ_max])
    count = np.array([numb_disk,numb_high,numb_hole,numb_infi,numb_over,numb_eqtr])

    return Image_G,Image_Y,Image_P,count,dQ
