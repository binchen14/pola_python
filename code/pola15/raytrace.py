#import numpy as np
#import scipy as sp
#import scipy.special as spsp
#from scipy.linalg      import norm
#from scipy.interpolate import interp1d
import pylab as pl
import time
from mod import *

#-----------------------------------------
M,  r_obs    = [1.0,   1E6]
a,  inc_disk = [0.998, np.radians(75)]
r_disk,x_size,y_size = [20.0, 25.0, 25.0]
nx, ny      =  [20,   20]
Gamma, nr   =  [2.0, 3.0]

#------------------------------------------
#   BRTP(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny)
t_start   = time.time()
Image_G,Image_Y,Image_Q,Image_P,count = BRTP(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny)
t_finish  = time.time()
run_time  = t_finish - t_start          # in seconds
numb_rays = (2*nx+1)*(2*ny+1)
rate_BRT  = float(numb_rays)/run_time

g_max     = Image_G.max()
g_min     = Image_G[Image_G.nonzero()].min()
err_Q_max = Image_Q.max()
err_Q_min = Image_Q[Image_Q.nonzero()].min()

#-------------------------------------------------------------------
# compute the strong lensing magnification to specific flux and image area
# intensity profile: I_nu = 1/(r**nr*nu**(Gamma-1))

dA_grid  =  x_size*y_size/float(nx*ny)
r_ISCO   =  r_MS_plus(M,a)
mu_obs   =  np.cos(inc_disk)
w_obs    =  f1(mu_obs)
t0       =  2.0*np.pi*mu_obs*w_obs        # a temporary variable

if np.abs(nr - 2.0) < 1E-6 :
    F_flat = t0*np.log(r_disk/r_ISCO)
else:
    F_flat = t0*np.abs( (r_ISCO**(2.0-nr) - r_disk**(2.0-nr))/(nr-2.0) )

area_flat = np.pi*np.abs(r_disk*r_disk - r_ISCO*r_ISCO)*mu_obs

F_lensed     = 0.0
area_lensed  = 0.0
for i in range(2*ny + 1):
    for j in range(2*nx + 1):
        if Image_G[i,j] > 1E-15:                # % a good shot
            g_ij = Image_G[i,j]                 # % read in the redshift
            r_ij = Image_Y[i,j,1]               # % read in the r coordinate
            w_ij = Image_P[i,j,0]               # % read the angular part
            dF   = w_ij*g_ij**(Gamma + 2.0)/r_ij**nr
            F_lensed = F_lensed + dF
            area_lensed = area_lensed + 1.0

F_lensed    *= dA_grid
area_lensed *= dA_grid
mu_flux      = F_lensed/F_flat                  # strong lensing magnification
mu_area      = area_lensed/area_flat

#-------------------------------------------------------------------
# compute specific intensity and Stokes parameters

I_total = 0.0                           # % total intensity initialization
Q_total = 0.0                           # % Stokes parameter U
U_total = 0.0                           # % Stokes parameter V
Image_I = np.zeros((2*ny+1,2*nx+1))     # image specific intensity in !!!! log10 scale
for i in range(2*ny+1):
    for j in range(2*nx+1):
        if Image_G[i,j] > 1E-15:
            g_ij    = Image_G[i,j]
            r_ij    = Image_Y[i,j,1]
            [w_ij, del_ij, chi_ij] = Image_P[i,j,:]
            lens_ij = g_ij**(Gamma + 2.0)/r_ij**nr  # effect of Kerr lensing
            I_ij    = w_ij*lens_ij                  # intensity in one pixel
            dQ      = del_ij*I_ij*np.cos(2.0*chi_ij)
            dU      = del_ij*I_ij*np.sin(2.0*chi_ij)
            Q_total = Q_total + dQ
            U_total = U_total + dU
            I_total = I_total + I_ij
            Image_I[i,j] = np.log10(np.abs(I_ij))               # store Intensity in log scale

chi_total = 0.5*np.arctan(U_total/Q_total)                      #  radian
del_total = np.abs(U_total/(I_total*np.sin(2.0*chi_total)))     # degree of polarization

print "run time in seconds is ", run_time, ", and rate = ", rate_BRT
print "(gmax,gmin) = ", g_max, "  ", g_min
print "max error in Q = ", err_Q_max, "min error in Q = ", err_Q_min
print "mu_flux = ", mu_flux, ", and mu_area = ", mu_area
print "chi_total = ", chi_total, ", and del_total = ", del_total
print "numb_rays = ", numb_rays, "  count= ", count.sum()," missed ", numb_rays-count.sum()

pl.figure(1)
pl.imshow(Image_G)
#pl.savefig('./fig1.eps')
pl.show()


#---------- Generate Intensity and Polarization plot -----

# generate the sparse vector field data for plot.
ix = max(3, np.floor( (2*nx+1)/float(25) ) )        # say, I want 25 arrows in x-drection
iy = max(3, np.floor( (2*ny+1)/float(25) ) )
[X,Y] = np.meshgrid( np.arange(0,2*nx+1,ix), np.arange(0,2*ny+1,iy) )
U = np.zeros(X.shape)
V = U
i1 = 0
for i in np.arange(0,2*nx+1,ix):
    j1 = 0
    for j in np.arange(0,2*ny+1,iy) :
        if Image_G[j,i] > 1E-15 :
            delta = Image_P[j,i,1]
            chi   = Image_P[j,i,2]
            U[j1,i1] =  delta*np.cos(chi)
            V[j1,i1] = -delta*np.sin(chi)   #% the overplot with intensity will flip the y-direction
        j1 += 1
    i1 += 1
#-----------------------------------------------------------------

#pl.figure(2)
#pl.imshow(Image_I)
#pl.quiver(X,Y, U, V)
#pl.savefig('./fig2.eps')
#pl.show()

